import { Component, OnInit } from '@angular/core';
import { infos } from 'src/app/infos';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { Info } from '../../model';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {


  public gridView: GridDataResult;
  public items: any[] = infos;
  public mySelection: number[] = [1, 2];
  public pageSize = 10;
  public skip = 0;

  constructor() {
      this.loadItems();
  }

  public pageChange(event: PageChangeEvent): void {
      this.skip = event.skip;
      this.loadItems();

      // Optionally, clear the selection when paging
      // this.mySelection = [];
  }

  private loadItems(): void {
      this.gridView = {
          data: this.items.slice(this.skip, this.skip + this.pageSize),
          total: this.items.length
      };
  }

  public infos: any[] = infos;

    createNewInfo(): Info {
        return new Info();
    }

  ngOnInit(): void {
  }

}
