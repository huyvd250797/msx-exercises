import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateInvoicedComponent } from './create-invoiced.component';

describe('CreateInvoicedComponent', () => {
  let component: CreateInvoicedComponent;
  let fixture: ComponentFixture<CreateInvoicedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateInvoicedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateInvoicedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
