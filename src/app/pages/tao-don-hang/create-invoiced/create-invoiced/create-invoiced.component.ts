import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-invoiced',
  templateUrl: './create-invoiced.component.html',
  styleUrls: ['./create-invoiced.component.css']
})
export class CreateInvoicedComponent implements OnInit {


  public listCus: Array<string> = [
    "Đại lý ABC","Trung tâm XYZ"
  ];
  
  public listPay: Array<string> = [
    "Tiền Mặt","Chuyển Khoản (Ngân Hàng)"
  ];

  public listUnt: Array<string> = [
    "%","VNĐ"
  ];

  public value: Date = new Date();

  constructor() { }

  ngOnInit(): void {
  }

}
