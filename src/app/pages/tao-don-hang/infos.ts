export const infos = [
    {
        'productID': 1,
        'product': 'Paracetamol 500mg hộp 50 viên',
        'price':'20.000 VNĐ',
        'stored':'100',
        'discount':'0%',
        'addfee':'50%',
        'amount':'500',
        'unitPrice':'300.000 VNĐ',
        'total':'18.000.000 VNĐ'
    },
    {
        'productID': 2,
        'product': 'Panadol Extra hộp 180 viên',
        'price':'14.000 VNĐ',
        'stored':'100',
        'discount':'0%',
        'addfee':'50%',
        'amount':'200',
        'unitPrice':'300.000 VNĐ',
        'total':'18.000.000 VNĐ'
    },
    {
        'productID': 3,
        'product': 'Panadol Extra hộp 50 viên',
        'price':'10.000 VNĐ',
        'stored':'20',
        'discount':'1%',
        'addfee':'50%',
        'amount':'500',
        'unitPrice':'300.000 VNĐ',
        'total':'18.000.000 VNĐ'
    },
    {
        'productID': 4,
        'product': 'Paracetamol 500mg hộp 100 viên',
        'price':'29.000 VNĐ',
        'stored':'100',
        'discount':'0%',
        'addfee':'40%',
        'amount':'150',
        'unitPrice':'500.000 VNĐ',
        'total':'20.000.000 VNĐ'
    },
    {
        'productID': 5,
        'product': 'Paracetamol 500mg hộp 100 viên',
        'price':'54.000 VNĐ',
        'stored':'200',
        'discount':'0%',
        'addfee':'40%',
        'amount':'350',
        'unitPrice':'600.000 VNĐ',
        'total':'20.000.000 VNĐ'
    },
    
    

    
]
