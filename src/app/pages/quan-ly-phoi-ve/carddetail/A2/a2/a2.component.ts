import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-a2',
  templateUrl: './a2.component.html',
  styleUrls: ['./a2.component.css']
})
export class A2Component {
    //toggle
    public checked = true;
    public disabled = true;
}
