import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-a3',
  templateUrl: './a3.component.html',
  styleUrls: ['./a3.component.css']
})
export class A3Component {
      //toggle
      public checked = true;
      public disabled = true;
}
