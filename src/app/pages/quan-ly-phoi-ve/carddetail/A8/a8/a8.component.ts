import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-a8',
  templateUrl: './a8.component.html',
  styleUrls: ['./a8.component.css']
})
export class A8Component {
    //toggle
    public checked = true
    public disabled = true;
}
