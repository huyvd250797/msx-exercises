import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-a5',
  templateUrl: './a5.component.html',
  styleUrls: ['./a5.component.css']
})
export class A5Component {
    //toggle
    public checked = true;
    public disabled = true;
}
