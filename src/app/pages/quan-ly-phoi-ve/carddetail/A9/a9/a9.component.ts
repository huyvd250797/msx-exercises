import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-a9',
  templateUrl: './a9.component.html',
  styleUrls: ['./a9.component.css']
})
export class A9Component {
    //toggle
    public checked = true;
    public disabled = true;
}
