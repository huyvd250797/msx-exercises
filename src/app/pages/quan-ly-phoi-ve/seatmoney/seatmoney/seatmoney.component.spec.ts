import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeatmoneyComponent } from './seatmoney.component';

describe('SeatmoneyComponent', () => {
  let component: SeatmoneyComponent;
  let fixture: ComponentFixture<SeatmoneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeatmoneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeatmoneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
