import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent {
  public listTour: Array<string> = [
      'Sài Gòn - Đà Lạt', 'Sài Gòn - Vũng Tàu', 'Sài Gòn - Phan Thiết'
  ];

  public listNumPlate: Array<string> = [
    '59H1 - 9999','51A1 - 8888','51F3 - 7777'
  ];
  //get day time
  public value: Date = new Date();
  
  //toggle
  public checked = true;
}
