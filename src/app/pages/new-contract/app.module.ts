import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DropDownsModule, } from '@progress/kendo-angular-dropdowns';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { GridModule } from '@progress/kendo-angular-grid';
import { LabelModule } from '@progress/kendo-angular-label';
import { SortableModule } from '@progress/kendo-angular-sortable';
import { SchedulerModule } from '@progress/kendo-angular-scheduler';


import { InfosService } from './infos.service';

import { ContractComponent } from './contract/contract/contract.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';











@NgModule({
  declarations: [
    AppComponent,
    ContractComponent,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DropDownsModule,
    BrowserAnimationsModule,
    ButtonsModule,
    DateInputsModule,
    FormsModule,
    LabelModule,
    InputsModule,
    RouterModule.forChild(
      [{
          path: '',
          component: AppComponent
      }]
  ),
  CommonModule,
  LayoutModule,
  InputsModule,
  GridModule,
  SortableModule,
  SchedulerModule,
  NgxIntlTelInputModule,
  ReactiveFormsModule,

  ],
  providers: [InfosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
