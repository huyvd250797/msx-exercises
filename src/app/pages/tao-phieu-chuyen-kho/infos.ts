export const infos = [
    {
        'productID': 1,
        'product': 'Paracetamol 500mg hộp 50 viên',
        'amount': '600',
        'detailAmount': '20',
        'batchNo':'SX202005',
        'doproduce':'01/05/2020',
        'expiry':'01/05/2023',
        'stored':'100',
        'available':'24',
        'package':'KH001'
    },
    {
        'productID': 2,
        'product': 'Panadol Extra hộp 180 viên',
        'amount': '180',
        'detailAmount': '41',
        'batchNo':'SX202005',
        'doproduce':'03/05/2020',
        'expiry':'03/05/2023',
        'stored':'20',
        'available':'5',
        'package':'KH002'
    },
    {
        'productID': 3,
        'product': 'Paracetamol 500mg hộp 50 viên',
        'amount': '600',
        'detailAmount': '20',
        'batchNo':'SX202005',
        'doproduce':'01/05/2020',
        'expiry':'01/05/2023',
        'stored':'100',
        'available':'24',
        'package':'KH003'
        
    },
    {
        'productID': 4,
        'product': 'Panadol Extra hộp 180 viên',
        'amount': '180',
        'detailAmount': '41',
        'batchNo':'SX202005',
        'doproduce':'03/05/2020',
        'expiry':'03/05/2023',
        'stored':'20',
        'available':'5',
        'package':'KH004'
    },
    
    

    
]
