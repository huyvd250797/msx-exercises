import { Component, OnInit } from '@angular/core';
import { infos } from 'src/app/infos';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { Info } from '../../model';

@Component({
  selector: 'app-delivery-note',
  templateUrl: './delivery-note.component.html',
  styleUrls: ['./delivery-note.component.css']
})
export class DeliveryNoteComponent implements OnInit {

  public listFrSto: Array<string> = [
    "Kho Tân Bình","Kho Quận 1"
  ];

  public listToSto: Array<string> = [
    "Kho Quận 5","Kho Quận 7"
  ];
  public listCata: Array<string> = [
    "Thuốc Tiêu Hóa","Thuốc Hạ Sốt"
  ];
  public listProd: Array<string> = [
    "Paracetamol 500mg hộp 50 viên","Panadol Extra hộp 180 viên"
  ];

  public value: Date = new Date();

  public gridView: GridDataResult;
  public items: any[] = infos;
  public mySelection: number[] = [];
  public pageSize = 10;
  public skip = 0;

  constructor() {
      this.loadItems();
  }

  public pageChange(event: PageChangeEvent): void {
      this.skip = event.skip;
      this.loadItems();

      // Optionally, clear the selection when paging
      // this.mySelection = [];
  }

  private loadItems(): void {
      this.gridView = {
          data: this.items.slice(this.skip, this.skip + this.pageSize),
          total: this.items.length
      };
  }

  public infos: any[] = infos;

    createNewInfo(): Info {
        return new Info();
    }
    ngOnInit(): void {
    }

}
