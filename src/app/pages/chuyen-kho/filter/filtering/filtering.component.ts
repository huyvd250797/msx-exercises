import { Component, OnInit } from '@angular/core';
import { medicine } from '../../medicine';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

@Component({
  selector: 'app-filtering',
  templateUrl: './filtering.component.html',
  styleUrls: ['./filtering.component.css']
})
export class FilteringComponent {
    
    public gridView: GridDataResult;
    public items: any[] = medicine;
    public mySelection: number[] = [2, 3];
    public pageSize = 10;
    public skip = 0;

    constructor() {
        this.loadItems();
    }

    public pageChange(event: PageChangeEvent): void {
        this.skip = event.skip;
        this.loadItems();

        // Optionally, clear the selection when paging
        // this.mySelection = [];
    }

    private loadItems(): void {
        this.gridView = {
            data: this.items.slice(this.skip, this.skip + this.pageSize),
            total: this.items.length
        };
    }
}
