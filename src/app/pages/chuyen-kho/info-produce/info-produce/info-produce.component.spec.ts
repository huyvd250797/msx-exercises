import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoProduceComponent } from './info-produce.component';

describe('InfoProduceComponent', () => {
  let component: InfoProduceComponent;
  let fixture: ComponentFixture<InfoProduceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoProduceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoProduceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
