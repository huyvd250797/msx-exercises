import { Component, OnInit } from '@angular/core';
import { medicineselect } from '../../medicine';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';


@Component({
  selector: 'app-select-filter',
  templateUrl: './select-filter.component.html',
  styleUrls: ['./select-filter.component.css']
})
export class SelectFilterComponent {
  public autoCorrect: boolean = false;
  public value: number = 20;

  public listMedicine: Array<string> = [
    'SX202006','SX202005'
];
  
  public gridView: GridDataResult;
  public items: any[] = medicineselect;
  public mySelection: number[] = [];
  public pageSize = 10;
  public skip = 0;

  constructor() {
      this.loadItems();
  }

  public pageChange(event: PageChangeEvent): void {
      this.skip = event.skip;
      this.loadItems();

      // Optionally, clear the selection when paging
      // this.mySelection = [];
  }

  private loadItems(): void {
      this.gridView = {
          data: this.items.slice(this.skip, this.skip + this.pageSize),
          total: this.items.length
      };
  }
}
