import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonsModule } from '@progress/kendo-angular-buttons';


import { DateInputsModule } from '@progress/kendo-angular-dateinputs';

import { FormsModule } from '@angular/forms';
import { InputsModule } from '@progress/kendo-angular-inputs';

import { LayoutModule } from '@progress/kendo-angular-layout';

import { GridModule } from '@progress/kendo-angular-grid';
import { InfoProduceComponent } from './info-produce/info-produce/info-produce.component';
import { InfoFilterComponent } from './info-filter/info-filter/info-filter.component';
import { FilteringComponent } from './filter/filtering/filtering.component';
import { SelectFilterComponent } from './select-filter/select-filter/select-filter.component';
import { LabelModule } from '@progress/kendo-angular-label';




@NgModule({
  declarations: [
    AppComponent,
    InfoProduceComponent,
    InfoFilterComponent,
    FilteringComponent,
    SelectFilterComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DropDownsModule,
    BrowserAnimationsModule,
    ButtonsModule,
    DateInputsModule,
    FormsModule,
    LabelModule,
    InputsModule,
    RouterModule.forChild(
      [{
          path: '',
          component: AppComponent
      }]
  ),
  CommonModule,
  LayoutModule,
  InputsModule,
  GridModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
