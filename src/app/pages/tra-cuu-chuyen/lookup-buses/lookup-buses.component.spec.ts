import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookupBusesComponent } from './lookup-buses.component';

describe('LookupBusesComponent', () => {
  let component: LookupBusesComponent;
  let fixture: ComponentFixture<LookupBusesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookupBusesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookupBusesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
