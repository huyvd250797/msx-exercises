import { Component, OnInit } from '@angular/core';
import { infos } from 'src/app/infos';


@Component({
  selector: 'app-lookup-buses',
  templateUrl: './lookup-buses.component.html',
  styleUrls: ['./lookup-buses.component.css']
})
export class LookupBusesComponent implements OnInit {


  public checked = true;
  public disabled = true;

  students = infos;

  constructor() { }

  ngOnInit(): void {
  }

}
