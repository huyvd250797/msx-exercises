export class Info {
    public producID: number;
    public product = '';
    public amount: '';
    public detailAmount: '';
    public batchNo: '';
    public doproduce: '';
    public expiry: '';
    public stored: '';
    public available: '';
}
