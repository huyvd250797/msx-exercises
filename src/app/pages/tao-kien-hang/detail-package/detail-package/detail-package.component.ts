import { Component, OnInit, OnDestroy, ViewChild, Renderer2 } from '@angular/core';
import { infos } from '../../infos'

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InfosService } from '../../infos.service';
import { AddEvent, EditEvent, GridComponent } from '@progress/kendo-angular-grid';
import { groupBy, GroupDescriptor } from '@progress/kendo-data-query';

const createFormGroup = dataItem => new FormGroup({

  'productID': new FormControl(dataItem.productID),
  'batchNo': new FormControl(dataItem.batchNo),
  'product': new FormControl(dataItem.product, Validators.required),
  'doproduce': new FormControl(dataItem.doproduce),
  'expiry': new FormControl(dataItem.expiry),
  'stored': new FormControl(dataItem.stored),
  'detailAmount':  new FormControl(dataItem.detailAmount, Validators.compose([Validators.required, Validators.pattern('^[0-9]{1,3}')]))

});
const matches = (el, selector) => (el.matches || el.msMatchesSelector).call(el, selector);


@Component({
  selector: 'app-detail-package',
  templateUrl: './detail-package.component.html',
  styleUrls: ['./detail-package.component.css']
})
export class DetailPackageComponent implements OnInit, OnDestroy {


  public gridData1: any[] = infos;

  

  @ViewChild(GridComponent)
    private grid: GridComponent;


    
    public groups: GroupDescriptor[] = [];
    public view: any[];

    public formGroup: FormGroup;

    private editedRowIndex: number;
    private docClickSubscription: any;
    private isNew: boolean;

    constructor(private service: InfosService, private renderer: Renderer2, public infoService: InfosService) { }

    public ngOnInit(): void {
      this.view = this.service.infos();

      this.docClickSubscription = this.renderer.listen('document', 'click', this.onDocumentClick.bind(this));
    }

    public ngOnDestroy(): void {
        this.docClickSubscription();
    }

    public addHandler(): void {
        this.closeEditor();

        this.formGroup = createFormGroup({
          'product':'',
          'batchNo':'',
          'doproduce':'',
          'expiry':'',
          'stored': 0,
          'detailAmount': 0,
          

        });
        this.isNew = true;

        this.grid.addRow(this.formGroup);
    }

    public removeHandler({ sender, dataItem }) {
        this.infoService.remove(dataItem);

        sender.cancelCell();
    }

    public cellClickHandler({ isEdited, dataItem, rowIndex }): void {
        if (isEdited || (this.formGroup && !this.formGroup.valid)) {
            return;
        }

        if (this.isNew) {
            rowIndex += 1;
        }

        this.saveCurrent();

        this.formGroup = createFormGroup(dataItem);
        this.editedRowIndex = rowIndex;

        this.grid.editRow(rowIndex, this.formGroup);
    }

    public cancelHandler(): void {
        this.closeEditor();
    }

    public groupChange(groups: GroupDescriptor[]): void {
        this.groups = groups;
        this.view = groupBy(this.service.infos(), this.groups);
    }

    private closeEditor(): void {
        this.grid.closeRow(this.editedRowIndex);

        this.isNew = false;
        this.editedRowIndex = undefined;
        this.formGroup = undefined;
    }

    private onDocumentClick(e: any): void {
        if (this.formGroup && this.formGroup.valid &&
            !matches(e.target, '#infosGrid tbody *, #infosGrid .k-grid-toolbar .k-button')) {
            this.saveCurrent();
        }
    }

    private saveCurrent(): void {
        if (this.formGroup) {
            this.service.save(this.formGroup.value, this.isNew);
            this.closeEditor();
        }
    }
}
