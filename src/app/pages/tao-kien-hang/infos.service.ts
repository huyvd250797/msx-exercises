import { Injectable } from '@angular/core';
import { infos } from './infos';

@Injectable()
export class InfosService {
    private data: any[] = infos;
    private counter: number = infos.length;

    public infos(): any[] {
        return this.data;
    }

    public remove(info: any): void {
        const index = this.data.findIndex(({ productID }) => productID === info.productID);
        this.data.splice(index, 1);
    }

    public save(info: any, isNew: boolean): void {
        if (isNew) {
            info.productID = this.counter++;
            this.data.splice(0, 0, info);
        } else {
            Object.assign(
                this.data.find(({ productID }) => productID === info.productID),
                info
            );
        }
    }
}
