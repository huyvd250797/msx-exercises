import { Component, OnInit, OnDestroy, ViewChild, Renderer2 } from '@angular/core';
import { infos } from '../../infos'





@Component({
  selector: 'app-package',
  templateUrl: './package.component.html',
  styleUrls: ['./package.component.css']
})
export class PackageComponent implements OnInit {

  public gridData: any[] = infos;
  
  public listOrd: Array<string> = [
    "DH001","DH002"
  ];

  public listCus: Array<string> = [
    "Đại lý ABC","Trung tâm XYZ"
  ];

  public listTMed: Array<string> = [
    "Thuốc hạ sốt","Thuốc tiêu hóa"
  ];

  public listMed: Array<string> = [
    "Paracetamol 500mg hộp 50 viên","Panadol Extra hộp 180 viên"
  ];

  constructor() { }

  ngOnInit(): void {
  }

  
}
