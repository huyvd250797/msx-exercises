export const infos = [
    {
        'productID':'1',
        'product': 'Paracetamol 500mg hộp 50 viên',
        'amount': '600',
        'detailAmount': '20',
        'batchNo':'SX202005',
        'doproduce':'01/05/2020',
        'expiry':'01/05/2023',
        'stored':'100',
        'available':'24'
    },
    {
        'productID':'2',
        'product': 'Panadol Extra hộp 180 viên',
        'amount': '180',
        'detailAmount': '41',
        'batchNo':'SX202005',
        'doproduce':'03/05/2020',
        'expiry':'03/05/2023',
        'stored':'20',
        'available':'5'
    },
    {
        'productID':'3',
        'product': 'Paracetamol 500mg hộp 50 viên',
        'amount': '600',
        'detailAmount': '20',
        'batchNo':'SX202005',
        'doproduce':'01/05/2020',
        'expiry':'01/05/2023',
        'stored':'100',
        'available':'0'
    },
    {
        'productID':'4',
        'product': 'Panadol Extra hộp 180 viên',
        'amount': '180',
        'detailAmount': '35',
        'batchNo':'SX202005',
        'doproduce':'03/05/2020',
        'expiry':'03/05/2023',
        'stored':'20',
        'available':'0'
    },
    {
        'productID':'5',
        'product': 'Paracetamol 500mg hộp 50 viên',
        'amount': '600',
        'detailAmount': '20',
        'batchNo':'SX202005',
        'doproduce':'01/05/2020',
        'expiry':'01/05/2023',
        'stored':'100',
        'available':'200'
    },
    {
        'productID':'6',
        'product': 'Panadol Extra hộp 180 viên',
        'amount': '180',
        'detailAmount': '20',
        'batchNo':'SX202005',
        'doproduce':'03/05/2020',
        'expiry':'03/05/2023',
        'stored':'800',
        'available':'300'
    },
    
    

    
]
