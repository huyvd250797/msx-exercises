import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { FormsModule } from '@angular/forms';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { GridModule } from '@progress/kendo-angular-grid';
import { LabelModule } from '@progress/kendo-angular-label';
import { SortableModule } from '@progress/kendo-angular-sortable';
import { SchedulerModule } from '@progress/kendo-angular-scheduler';


import { InfosService } from './infos.service';

import { StepByStepComponent } from './ng-wizard/step-by-step/step-by-step.component';

import { NgWizardModule, NgWizardConfig, THEME } from 'ng-wizard';
import { Step1Component } from './step1/step1/step1.component';
import { Step2Component } from './step2/step2/step2.component';
 
const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default
};











@NgModule({
  declarations: [
    AppComponent,
    StepByStepComponent,
    Step1Component,
    Step2Component,


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DropDownsModule,
    BrowserAnimationsModule,
    ButtonsModule,
    DateInputsModule,
    FormsModule,
    LabelModule,
    InputsModule,
    RouterModule.forChild(
      [{
          path: '',
          component: AppComponent
      }]
  ),
  CommonModule,
  LayoutModule,
  InputsModule,
  GridModule,
  SortableModule,
  SchedulerModule,
  NgWizardModule.forRoot(ngWizardConfig)
  ],
  providers: [InfosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
