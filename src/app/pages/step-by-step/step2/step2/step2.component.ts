import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.css']
})
export class Step2Component implements OnInit {

  public value: Date = new Date();

  public listGender: Array<string> = [
    "Nam","Nữ","Khác"
  ];
  
  public listCountry: Array<string> = [
    "Việt Nam", "Nhật Bản", "Thái Lan"
  ];
  
  public listDep: Array<string> = [
    "Phòng Nhân Sự", "Phòng Kế Toán", "Phòng IT"
  ];
  
  public listCity: Array<string> = [
    "Tp. Hồ Chí Minh", "Đồng Nai", "Vũng Tàu"
  ];
  
  public listPosi: Array<string> = [
    "Trưởng Phòng", "Giám Đốc", "Nhân Viên"
  ];
  
  constructor() { }

  ngOnInit(): void {
  }

}
