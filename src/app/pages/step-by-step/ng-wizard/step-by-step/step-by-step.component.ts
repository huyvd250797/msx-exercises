import { Component, OnInit } from '@angular/core';

import { NgWizardConfig, THEME, StepChangedArgs, NgWizardService } from 'ng-wizard';


@Component({
  selector: 'app-step-by-step',
  templateUrl: './step-by-step.component.html',
  styleUrls: ['./step-by-step.component.css']
})
 
export class StepByStepComponent implements OnInit {
 


  config: NgWizardConfig = {
    selected: 0,
    theme: THEME.arrows,
    toolbarSettings: {
      toolbarExtraButtons: [
        { text: 'Save Changes', class: 'btn btn-success', event: () => { alert("Saved."); } },
        { text: 'Reset', class: 'btn btn-danger', event: () => { this.resetWizard(); } },
      ]
    }
  };
 
  constructor(private ngWizardService: NgWizardService) {
  }
 
  ngOnInit() {
  }
 
  showPreviousStep(event?: Event) {
    this.ngWizardService.previous();
  }
 
  showNextStep(event?: Event) {
    this.ngWizardService.next();
  }
 
  resetWizard(event?: Event) {
    this.ngWizardService.reset();
  }
 
  setTheme(theme: THEME) {
    this.ngWizardService.theme(theme);
  }
 
  stepChanged(args: StepChangedArgs) {
    console.log(args.step);
  }
}
