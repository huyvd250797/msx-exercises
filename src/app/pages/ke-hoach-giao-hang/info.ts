export const Info = [

    {
        'product': 'Thuốc S',
        'stored': '153',
        'totalInvoive': '38',
        'invoiceItems': [
            {
            'fullname': 'Nguyễn Văn S',
            'total': '20',
            'values' : [
                {'value': '3'},
                {'value': '9'},
                {'value': ''},
            ] ,
            },
            {
            'fullname': 'Phạm Thị S',
            'total':'18',
            'values': [
                {'value': '8' },
                {'value': ''},
                {'value': '11'},
            ],
            },
        ],
    },

    {
        'product': 'Thuốc A',
        'stored': '1000',
        'totalInvoive': '38',
        'invoiceItems': [
            {
            'fullname': 'Nguyễn Văn A',
            'total': '20',
            'values': [
                {'value': '1' },
                {'value': ''},
            ],
            },
            {
            'fullname': 'Phạm Thị B',
            'total':'18',
            'values': [
                {'value': '' },
                {'value': '14'},
            ],
            }
        ],
    },

    {
        'product': 'Thuốc B',
        'stored': '5000',
        'totalInvoive': '27',
        'invoiceItems': [
            {
            'fullname': 'Phạm Thị C',
            'total': '14',
            'values' : '4'
            },
            {
            'fullname': 'Trần Văn D',
            'total':'27',
            'values' : '5' ,
            },
        ],
    },
    {
        'product': 'Thuốc C',
        'stored': '9000',
        'totalInvoive': '64',
        'invoiceItems': [
            {
            'fullname': 'Nguyễn Văn E',
            'total': '23',
            'values' : ''
            },
            {
                'fullname': 'Nguyễn Văn H',
                'total': '5',
                'values' : ''
                },
            {
            'fullname': 'Phạm Thị K',
            'total':'10',
            'values' : '' ,
            },
        ],
    },
]
// export const info = [
// {
//     'product': 'Thuốc A',
//     'stored': '1000',
//     'sumDH': '38',
//     'fullname': 'Nguyễn Văn A',
//     'sum-order': '20',
// },
// {
//     'product': 'Thuốc B',
//     'stored': '1000',
//     'sumDH': '38',
//     'fullname': 'Nguyễn Văn A',
//     'sum-order': '20',
// },
// ]