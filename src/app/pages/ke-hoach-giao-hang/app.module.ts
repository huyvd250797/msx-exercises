import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonsModule } from '@progress/kendo-angular-buttons';


import { DateInputsModule } from '@progress/kendo-angular-dateinputs';

import { FormsModule } from '@angular/forms';
import { InputsModule } from '@progress/kendo-angular-inputs';

import { LayoutModule } from '@progress/kendo-angular-layout';

import { GridModule } from '@progress/kendo-angular-grid';

import { LabelModule } from '@progress/kendo-angular-label';
import { DropdownlistComponent } from './dropdownlist/dropdownlist/dropdownlist.component';
import { FilteringComponent } from './filtering/filtering/filtering.component';
import { SortableModule } from '@progress/kendo-angular-sortable';
import { DetailsComponent } from './details/details/details.component';
import { SchedulerModule } from '@progress/kendo-angular-scheduler';










@NgModule({
  declarations: [
    AppComponent,
    DropdownlistComponent,
    FilteringComponent,
    DetailsComponent, 


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DropDownsModule,
    BrowserAnimationsModule,
    ButtonsModule,
    DateInputsModule,
    FormsModule,
    LabelModule,
    InputsModule,
    RouterModule.forChild(
      [{
          path: '',
          component: AppComponent
      }]
  ),
  CommonModule,
  LayoutModule,
  InputsModule,
  GridModule,
  SortableModule,
  SchedulerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
