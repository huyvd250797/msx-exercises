import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';


interface ColumnSetting {
  field: string;
  title: string;
}


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnChanges{
  
  @Input() public detailts  = [];

  
  ngOnChanges(changes: SimpleChanges): void {
  console.log(this.detailts);
  }

  public columns: ColumnSetting[] = [
    {
      field: 'values.0.value',
      title: '1',

    }, {
      field: 'values.1.value',
      title: '2',

    }, {
      field: 'values.2.value',
      title: '3',
    },
    {
      field: '',
      title: '4',
    },
    {
      field: '',
      title: '5',
    },
    {
      field: '',
      title: '6',
    },
    {
      field: '',
      title: '7',
    },
    {
      field: '',
      title: '8',
    },
  ]
}
