import { Component, OnInit } from '@angular/core';
import { SelectionRange } from '@progress/kendo-angular-dateinputs';

@Component({
  selector: 'app-dropdownlist',
  templateUrl: './dropdownlist.component.html',
  styleUrls: ['./dropdownlist.component.css']
})
export class DropdownlistComponent  {
  public listMedicine: Array<string> = [
    "Thuốc A","Thuốc B"
  ];
  public listNameCus: Array<string> = [
    "Nguyễn Văn A","Phạm Thị B","Trần Anh C"
  ];
  public value: Date = new Date();
  public range: SelectionRange = { start: null, end: null };
}
