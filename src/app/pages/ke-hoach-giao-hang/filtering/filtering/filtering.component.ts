import { Component, OnInit } from '@angular/core';
import { Info } from 'src/app/info';


@Component({
  selector: 'app-filtering',
  templateUrl: './filtering.component.html',
  styleUrls: ['./filtering.component.css']
})
export class FilteringComponent implements OnInit{
  
  public gridData: any[]  = [];
  
  ngOnInit(): void {
    this.gridData = Info;
  }
}
