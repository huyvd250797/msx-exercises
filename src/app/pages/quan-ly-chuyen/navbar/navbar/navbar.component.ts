import { Component, OnInit } from '@angular/core';
import { data } from '../../data'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public checked = true;
  public disabled = true;

  cards = data;

  constructor() { }

  ngOnInit(): void {
  }

  
}

